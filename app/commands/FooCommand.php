<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FooCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'foo:generate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Description of foo';
    
    /**
     *
     * @var type 
     */
    private $testModel;


    /**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
        
        $this->testModel = new Test();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		//
        // Get the name arguments and the age option from the input instance.
        $name = $this->argument('name');
 
        $age = $this->option('age', 13);
 
        $this->line("{$name} is {$age} years old.\n\n");
        
        $this->line("THIS IS FIRST DEMO - GET ALL USERS FROM DATABASE");
        $arrDataUserResult = $this->testModel->getUser();
        
        foreach ($arrDataUserResult as $user)
        {
            $this->line($user->name);
        }
		
        $this->line("\n\n\n Update users ");
        $this->testModel->updateUser(array('name'=>$name), 5);
        
        $this->line("After update \n\n\n");
        $arrDataUser = $this->testModel->getUser();
        
        foreach ($arrDataUser as $user)
        {
            $this->line($user->name);
        }
        
       // if ($this->confirm('Do you wish to continue? [yes|no]'))
       //{
       //    $this->line("Hello\n");
       // }
        $this->line("FINISH\n\n");
        
        
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('name', InputArgument::REQUIRED, 'Name of the new user'/*,
                    'age', InputArgument::OPTIONAL, 'Age of the new user'*/
                ),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('age', null, InputOption::VALUE_REQUIRED, 'Age of the new user'),
		);
	}

}