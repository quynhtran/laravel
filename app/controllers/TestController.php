<?php

class TestController extends BaseController {

    /**
     * Show the profile for the given user.
     */
	 
	protected static $_data ;
	
	public function __construct(){
		$this->test = new Test;
	}
	
    public function show()
    {	

		self::$_data['users'] = $this->test->getUser();		
		
        return View::make('test',self::$_data);
    }
	
	public function insert()
	{
		if(isset($_POST['Submit'])){
			$this->test->insertUser($_POST['name'],$_POST['content']);
			//echo $_POST['name'].'<br/>'.$_POST['content'];
		}
		
		self::$_data['users'] = $this->test->getUser();	
		return View::make('test',self::$_data);
		
	}

}