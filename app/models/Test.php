<?php

class Test extends Eloquent  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

	
	public function getUser()
	{
		return DB::table('user')->get();
	}

	public function insertUser($name='',$content='')
	{
		DB::table('user')->insert(array('name' => $name, 'content' => $content));
	}
    
    public function updateUser($data=array(),$id=0)
    {
        DB::table('user')
                ->where('id',$id)
                ->update(array('name'=>$data['name']));
    }
    
	
	

}