<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Laravel PHP Framework</title>
    <style>
        @import url(//fonts.googleapis.com/css?family=Lato:300,400,700);

        body {
            margin:0;
            font-family:'Lato', sans-serif;
            text-align:center;
            color: #999;
        }

        .welcome {
           width: 300px;
           height: 300px;
           position: absolute;
           left: 50%;
           top: 50%; 
           margin-left: -150px;
           margin-top: -150px;
        }

        a, a:visited {
            color:#FF5949;
            text-decoration:none;
        }

        a:hover {
            text-decoration:underline;
        }

        ul li {
            display:inline;
            margin:0 1.2em;
        }

        p {
            margin:2em 0;
            color:#555;
        }
    </style>
</head>
<body>
    <div align="left">
		
          <form name="form1" method="post" action="useradd">
            <div align="left">
                <label>Name :</label>&nbsp;
                <input type="text" name="name" id="name" width="120">
			</div><br/><br/>
			<div align="left">
                <label>Intro :</label>&nbsp;
                <textarea name="content" cols="25" rows="5"></textarea>
			</div><br/><br/>
			<div align="left"><input type="submit" name="Submit" id="Submit" >
			</div>
            <br/>
			
			User list : <br/>
			<?php 
				$i = 1;
				foreach($users as $user){
					echo $i.'.&nbsp;'.$user->name.'<br/>';
					$i++;
				}
			?>
          </form>
    
</div>
</body>
</html>
